#!/bin/bash

#RECOPILAR DATOS DESDE API https://datahub.io/core/population/r/popultion.csv
wget https://datahub.io/core/population/r/population.csv

#LIMPIAR DATOS (FILTRANDO POR PAÍS "SPAIN")
grep 'Spain' population.csv | sed 's/,/\t/g' > data_spain.tsv

#VISUALIZAR DATOS (GRÁFICA DE LA POBLACIÓN EN FUNCIÓN DEL AÑO)
gnuplot -e "set terminal dumb; plot 'data_spain.tsv' using 3:4"
